package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by index.";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getTaskEndpoint().removeTaskByIndex(getSession(), index);
    }
}
