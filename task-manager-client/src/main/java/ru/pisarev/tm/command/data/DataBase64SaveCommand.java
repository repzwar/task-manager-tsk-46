package ru.pisarev.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.AuthAbstractCommand;

public class DataBase64SaveCommand extends AuthAbstractCommand {

    @Nullable

    public String name() {
        return "data-save-base64";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Save base64 data";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().saveDataBase64(getSession());
    }

}