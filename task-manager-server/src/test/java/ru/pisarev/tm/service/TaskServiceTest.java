package ru.pisarev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pisarev.tm.dto.TaskRecord;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.marker.DBCategory;
import ru.pisarev.tm.service.dto.TaskRecordService;

import java.util.List;

public class TaskServiceTest {

    @Nullable
    private TaskRecordService taskService;

    @Nullable
    private TaskRecord task;

    @Before
    public void before() {
        taskService = new TaskRecordService(new ConnectionService(new PropertyService()));
        task = taskService.add("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", new TaskRecord("Task"));
    }

    @Test
    @Category(DBCategory.class)
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("Task", task.getName());

        @NotNull final TaskRecord taskById = taskService.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        @NotNull final List<TaskRecord> tasks = taskService.findAll();
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserId() {
        @NotNull final List<TaskRecord> tasks = taskService.findAll("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce");
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<TaskRecord> tasks = taskService.findAll("test");
        Assert.assertNotEquals(1, tasks.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findById() {
        @NotNull final TaskRecord task = taskService.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrect() {
        @NotNull final TaskRecord task = taskService.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "34");
        Assert.assertNull(task);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @NotNull final TaskRecord task = taskService.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrectUser() {
        @NotNull final TaskRecord task = taskService.findById("test", this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        taskService.removeById(task.getId());
        Assert.assertNull(taskService.findById(task.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        taskService.removeById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        @NotNull final TaskRecord task = taskService.findByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "Task");
        Assert.assertNotNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrect() {
        @NotNull final TaskRecord task = taskService.findByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "34");
        Assert.assertNull(task);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyNameException.class)
    public void findByNameNull() {
        @NotNull final TaskRecord task = taskService.findByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrectUser() {
        @NotNull final TaskRecord task = taskService.findByName("test", this.task.getName());
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIndex() {
        @NotNull final TaskRecord task = taskService.findByIndex("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", 1);
        Assert.assertNotNull(task);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIndexException.class)
    public void findByIndexNull() {
        @NotNull final TaskRecord task = taskService.findByIndex("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void removeById() {
        taskService.removeById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", task.getId());
        Assert.assertNull(taskService.findById(task.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        taskService.removeById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIndexException.class)
    public void removeByIndexNull() {
        taskService.removeByIndex("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
    }


    @Category(DBCategory.class)
    @Test(expected = EmptyNameException.class)
    public void removeByNameNull() {
        taskService.removeByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
    }

}
