package ru.pisarev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pisarev.tm.dto.SessionRecord;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.system.AccessDeniedException;
import ru.pisarev.tm.marker.DBCategory;
import ru.pisarev.tm.service.dto.SessionRecordService;
import ru.pisarev.tm.service.dto.UserRecordService;

import java.util.List;

public class SessionServiceTest {

    @Nullable
    private SessionRecordService sessionService;

    @Nullable
    private SessionRecord session;

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final UserRecordService userService = new UserRecordService(connectionService, new PropertyService());
        userService.add("user", "user");
        sessionService = new SessionRecordService(
                connectionService,
                userService,
                new PropertyService());
        @NotNull final SessionRecord session = new SessionRecord();
        this.session = sessionService.add(session);
    }

    @Test
    @Category(DBCategory.class)
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("userId", session.getUserId());

        @NotNull final SessionRecord sessionById = sessionService.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session, sessionById);
    }

    @Test
    @Category(DBCategory.class)
    public void findById() {
        @NotNull final SessionRecord session = sessionService.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrect() {
        @NotNull final SessionRecord session = sessionService.findById("34");
        Assert.assertNull(session);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        sessionService.findById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserId() {
        @NotNull final List<SessionRecord> session = sessionService.findAllByUserId(this.session.getUserId());
        Assert.assertNotNull(session);
        Assert.assertEquals(1, session.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<SessionRecord> session = sessionService.findAllByUserId("34");
        Assert.assertNotNull(session);
        Assert.assertNotEquals(1, session.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdNull() {
        @NotNull final List<SessionRecord> session = sessionService.findAllByUserId(null);
        Assert.assertNull(session);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        sessionService.removeById(session.getId());
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        sessionService.removeById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeById() {
        sessionService.removeById(session.getId());
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        sessionService.removeById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void close() {
        sessionService.close(session);
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void closeAllByUserId() {
        sessionService.closeAllByUserId(session.getUserId());
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = AccessDeniedException.class)
    public void validateIncorrect() {
        sessionService.validate(session);
    }

    @Test
    @Category(DBCategory.class)
    public void open() {
        @NotNull final SessionRecord session = sessionService.open("user", "user");
        Assert.assertNotNull(session);
    }

    @Category(DBCategory.class)
    @Test(expected = AccessDeniedException.class)
    public void openIncorrect() {
        sessionService.open("user", "use");
    }

    @Test
    @Category(DBCategory.class)
    public void validate() {
        @NotNull final SessionRecord session = sessionService.open("user", "user");
        sessionService.validate(session);
    }

    @Category(DBCategory.class)
    @Test(expected = AccessDeniedException.class)
    public void validateChanged() {
        @NotNull final SessionRecord session = sessionService.open("user", "user");
        session.setSignature("7");
        sessionService.validate(session);
    }

}