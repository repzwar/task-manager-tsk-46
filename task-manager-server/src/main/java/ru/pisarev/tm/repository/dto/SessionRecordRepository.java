package ru.pisarev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.dto.ISessionRecordRepository;
import ru.pisarev.tm.dto.SessionRecord;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRecordRepository extends AbstractRecordRepository<SessionRecord> implements ISessionRecordRepository {

    public SessionRecordRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<SessionRecord> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM SessionDto e WHERE e.userId = :userId", SessionRecord.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM SessionDto e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @NotNull
    public List<SessionRecord> findAll() {
        return entityManager.createQuery("SELECT e FROM SessionDto e", SessionRecord.class).getResultList();
    }

    public SessionRecord findById(@Nullable final String id) {
        return entityManager.find(SessionRecord.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionDto e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        SessionRecord reference = entityManager.getReference(SessionRecord.class, id);
        entityManager.remove(reference);
    }
}