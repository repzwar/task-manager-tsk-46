package ru.pisarev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.repository.model.IUserRepository;
import ru.pisarev.tm.api.service.IConnectionService;
import ru.pisarev.tm.api.service.model.IUserService;
import ru.pisarev.tm.exception.empty.*;
import ru.pisarev.tm.exception.entity.EmailExistException;
import ru.pisarev.tm.exception.entity.LoginExistException;
import ru.pisarev.tm.exception.entity.UserNotFoundException;
import ru.pisarev.tm.model.UserGraph;
import ru.pisarev.tm.repository.model.UserGraphRepository;
import ru.pisarev.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public final class UserGraphService extends AbstractGraphService<UserGraph> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    public UserGraphService(
            @NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserGraph> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserGraphRepository(entityManager);
            return repository.findAll();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<UserGraph> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (UserGraph item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserGraph add(@Nullable final UserGraph entity) {
        if (entity == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserGraphRepository(entityManager);
            repository.add(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    private void update(@NotNull final UserGraph entity, @NotNull EntityManager entityManager) {
        @NotNull final IUserRepository repository = new UserGraphRepository(entityManager);
        repository.update(entity);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserGraph findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserGraphRepository(entityManager);
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserGraphRepository(entityManager);
            repository.clear();
            @NotNull final List<UserGraph> users = repository.findAll();
            for (UserGraph t :
                    users) {
                repository.remove(t);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserGraphRepository(entityManager);
            @Nullable final UserGraph user = repository
                    .getReference(optionalId.orElseThrow(EmptyIdException::new));
            if (user == null) return;
            repository.remove(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final UserGraph entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserGraphRepository(entityManager);
            @Nullable final UserGraph user = repository.getReference(entity.getId());
            if (user == null) return;
            repository.remove(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserGraph findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserGraphRepository(entityManager);
            return userRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public UserGraph findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserGraphRepository(entityManager);
            return userRepository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserRepository userRepository = new UserGraphRepository(entityManager);
            @Nullable final UserGraph user = userRepository.findByLogin(login);
            if (user == null) return;
            userRepository.remove(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph add(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new LoginExistException(login);
        @NotNull final UserGraph user = new UserGraph();
        user.setLogin(login);
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordHashException::new);
        user.setPasswordHash(passwordHash);
        add(user);

        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph add(
            @Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new LoginExistException(login);
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExist(email)) throw new EmailExistException(login);
        @NotNull final UserGraph user = new UserGraph();
        user.setLogin(login);
        user.setEmail(email);
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordHashException::new);
        user.setPasswordHash(passwordHash);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserRepository userRepository = new UserGraphRepository(entityManager);
            add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserGraph user = findById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordHashException::new);
        user.setPasswordHash(passwordHash);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserGraphRepository(entityManager);
            update(user, entityManager);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }


    @Override
    @SneakyThrows
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final UserGraph user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserGraphRepository(entityManager);
            update(user, entityManager);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph lockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserGraph user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserGraphRepository(entityManager);
            update(user, entityManager);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph unlockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserGraph user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserGraphRepository(entityManager);
            update(user, entityManager);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
