package ru.pisarev.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.ServiceLocator;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private static final int INTERVAL = 30;

    @NotNull
    private final ServiceLocator serviceLocator;

    public Backup(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        serviceLocator.getDataService().saveBackup();
    }

    public void load() {
        serviceLocator.getDataService().loadBackup();
    }

}
